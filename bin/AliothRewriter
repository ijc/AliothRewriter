#!/usr/bin/ruby

require 'toml-rb'
require 'pp'
require 'logger'
require 'erb'


def validate_config(config)
  needed_keys = {
      'general' => ['definitions', 'target']
  }

  needed_keys.each do |section, keys|
    for key in keys
      unless config.dig(section, key)
        $log.fatal("Could not find required config option '#{ key }' in section '#{ section }'")
        exit(1)
      end
    end
  end
end

def load_config()
  filename = '../aliothrewriter.toml'
  config_files = Array.new
  config_files.push("#{ENV['HOME']}/.config/#{ filename }")
  config_files.push(File.join(__dir__, filename))
  config_file = nil
  config_files.each{ |file|
    if File::file?(file) and File::readable?(file)
      config_file = file
      break
    end
  }
  unless config_file
    $log.fatal("Could not load configuration file, tried #{config_files.inspect}")
    exit(1)
  end
  $log.debug("Load configuration file #{ config_file }")
  config = TomlRB.load_file(config_file)
  validate_config(config)
  config
end

def parse_definitions()
  projects = Hash.new
  Dir.glob("#{ @config['general']['definitions'] }/*.conf").each do|f|
    $log.info("Parse definition file #{ f }")
    File.readlines(f).each do |line|
      next if line =~ /^(\s*#|$)/
      matches = /^([^\s]+)\s+([^\s]+)/.match(line)
      if matches
        $log.debug("Match line #{ line }")
        projects[matches[1]] = matches[2]
      else
        $log.warn("unmatched line #{ line }")
      end
    end
  end
  projects
end

$log = Logger.new(STDOUT)

if ENV['LOGLEVEL']
  $log.level = Logger.const_get(ENV['LOGLEVEL'])
else
  $log.level = Logger::WARN
end

@config = load_config

$log.level = Logger.const_get(@config.dig('log', 'level')) if @config.dig('log', 'level')

$log.debug("Parse definitions")
projects =  parse_definitions
template = File.read('templates/rewrite.erb')
$log.debug("Generate template")
result = ERB.new(template).result( binding )


File.write(@config['general']['target'], result)
